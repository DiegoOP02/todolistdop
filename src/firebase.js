// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCznCIVQgD2tE0LUrITlYfPD4o8j8FvuRo",
  authDomain: "todo-33f10.firebaseapp.com",
  projectId: "todo-33f10",
  storageBucket: "todo-33f10.appspot.com",
  messagingSenderId: "760814269686",
  appId: "1:760814269686:web:0586264c90899a881428d7",
  measurementId: "G-QGEG3NEBME"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const db = getFirestore(app)